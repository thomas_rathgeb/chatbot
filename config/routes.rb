Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root :to => "home#index"
  match '/search/' => "search#index" , :as => :search, :via => [:get, :post]

end
