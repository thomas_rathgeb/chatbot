# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
file = JSON.parse(File.read('dev-v2.0.json'))
articles = file["data"]
articles.each do |article|
  paragraphs = article["paragraphs"]
  paragraphs.each do |paragraph|
    questions = paragraph["qas"]
    questions.each do |question|
      unless question["is_impossible"]
        Question.create!(text: question['question'], answer: question['answers'][0]['text'])
      end
    end
  end
end
