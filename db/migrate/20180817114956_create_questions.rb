class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :text
      t.text :reduced
      t.text :stems
      t.text :synonyms
      t.text :metaphones
      t.text :answer
      t.timestamps
    end
  end
end
