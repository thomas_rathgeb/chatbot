class AddActualAnswer < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :actual_answer, :text
  end
end
