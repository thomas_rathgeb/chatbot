class SearchController < ApplicationController

  def index
    query = params[:q].strip
    trigram_limit = params[:trigram_limit]
    spell_check = params[:spell_check]

    @searches = SearchFactory.searches( {query: query, trigram_limit: trigram_limit, spell_check: spell_check} )
  end

end
