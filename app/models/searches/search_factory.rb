class SearchFactory

  def self.searches options
    s = []

    s << RawTrigram.new("Raw Trigram", options)
    s << ReducedTrigram.new("Reduced Trigram", options)
    # s << ReducedTrigramWithSynonyms.new("Reduced Trigram with Synonyms", options)

    return s
  end
end
