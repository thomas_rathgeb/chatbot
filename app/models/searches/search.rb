class Search
  include Nlp

  attr_reader :name

  def initialize name, options
    @name = name
    @raw_query = options[:query]
    @spell_check = options[:spell_check] || false
    @search_engine = SearchEngine.new(options)
  end

  def run
    @search_engine.trigram_search actual_query
  end

  def actual_query
    if @spell_check
      @raw_query = Speller.spell_check(@raw_query)
    end
    @raw_query
  end
end
