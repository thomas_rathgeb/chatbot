class ReducedTrigramWithSynonyms < Search
  include Nlp

  def run
    results = []
    actual_query.each do |query|
      results += @search_engine.trigram_search(query, "reduced")
    end
    results = results.sort_by { |e| -e.similarity }.uniq { |e| e.text }[0..9]
    return results
  end

  def actual_query
    queries = []
    query = Cleaner.clean(@raw_query)
    queries << query
    query.split.each do |word|
      synonyms = Synonym.synonyms(word) || []
      Synonym.synonyms(word).each do |syn|
        queries << replace_word_in_sentence(query, word, syn)
      end
    end

    return queries
  end

  def replace_word_in_sentence sentence, word, new_word
    return sentence.sub word, new_word
  end

end
