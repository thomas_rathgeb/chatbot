class ReducedTrigram < Search
  include Nlp

  def run
    @search_engine.trigram_search actual_query, "reduced"
  end

  def actual_query
    query = super
    Cleaner.clean(query)
  end
end
