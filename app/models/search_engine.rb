class SearchEngine

  def initialize options
    @trigram_limit = options[:trigram_limit] || 0.3
  end

  def trigram_search query, field="text"
    results = Question.select("questions.*, similarity(#{field}, #{ActiveRecord::Base.connection.quote(query)}) as similarity")
      .where("similarity(#{field}, ?) > #{@trigram_limit}", query)
      .order("similarity(text, #{ActiveRecord::Base.connection.quote(query)}) DESC")
      .limit(10)
    return results
  end


end
