class Question < ActiveRecord::Base
  include Nlp

  after_create :analyze

  def analyze
    self.reduced = Cleaner.clean(text)
    self.stems = Stemmer.stemm(self.reduced)
    self.metaphones = Metaphoner.metaphone(self.reduced)
    # self.synonyms = BigHugeThesaurus.synonyms( self.reduced )
    self.save
  end

  # def answer
  #   ReducedTrigram.new("Reduced Trigram", {query: self.})
  #
  # end

end
